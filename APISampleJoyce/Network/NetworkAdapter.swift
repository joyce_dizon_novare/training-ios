//
//  NetworkAdapter.swift
//  APISampleJoyce
//
//  Created by Joyce Dizon on 05/02/2019.
//  Copyright © 2019 Novare Technologies. All rights reserved.
//

import Foundation
import Moya

struct NetworkAdapter {
    
    static let provider = MoyaProvider<Imagga>()
    
    static func getTags(imageString: String, completion: @escaping ([TagElement]) -> ()) {
        provider.request(.tags(imageString: imageString)) { result in
            switch result {
            case let .success(response):
                do {
                    let imageResult = try JSONDecoder().decode(ImageRecognitionResult.self, from: response.data)
                    completion (imageResult.result.tags)
                } catch let err {
                    print (err)
                }
                
            case let .failure(error):
                print (error)
            }
        }
    }
}
