//
//  Imagga.swift
//  APISampleJoyce
//
//  Created by Joyce Dizon on 05/02/2019.
//  Copyright © 2019 Novare Technologies. All rights reserved.
//

import Foundation
import Moya

enum Imagga {
    static private let auth = "YWNjX2Q0ZWY5NzE4MjljNGE0Mzo5N2E2ZWM2N2U5YjM5MWEzNDFhYTljNmJiZjdiNmM1ZQ=="
    case tags (imageString: String)
}

extension Imagga: TargetType {
    
    public var baseURL: URL {
        return URL(string: "https://api.imagga.com/v2")!
//        return URL(string: "https://joycedizon-eval-test.apigee.net")!

    }
    
    public var path: String {
        switch self {
        case .tags:
            return "/tags"
//            return "/Imagga"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .tags:
            return .post
        }
    }
    
    public var task: Task {
        switch self {
        case .tags (let imageString):
            return .requestParameters(parameters: ["image_base64" : imageString], encoding: URLEncoding.default)
        }
    }
    
    public var headers: [String: String]? {
        return ["Authorization": "Basic \(Imagga.auth)"]
    }
    
    var sampleData: Data {
        return Data()
    }

}
