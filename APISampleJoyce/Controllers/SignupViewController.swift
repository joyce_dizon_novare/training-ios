//
//  SignupViewController.swift
//  APISampleJoyce
//
//  Created by Joyce Dizon on 17/02/2019.
//  Copyright © 2019 Novare Technologies. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class SignupViewController: UIViewController {
        
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmTextField: UITextField!
    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//    }
    
    @IBAction func signup(_ sender: UIButton) {
        validateFields()
    }
    
    @IBAction func cancelSignUp(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

    func validateFields(){
        if  (emailTextField.text?.isEmpty)! ||
            (passwordTextField.text?.isEmpty)! ||
            (confirmTextField.text?.isEmpty)! {
            displayAlert(alertMessage: "Please fill up all fields.")
            return
        }
        if ((passwordTextField.text?.elementsEqual(confirmTextField.text!)) != true) {
            displayAlert(alertMessage: "Please make sure passwords match.")
            return
        }
        createAccount()
    }
    
    func displayAlert(alertMessage:String) -> Void {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: "Alert", message: alertMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default) {
                (action:UIAlertAction!) in
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: nil)
                }
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
//    func showActivityIndicator() {
//        let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
//        activityIndicator.center = view.center
//        activityIndicator.hidesWhenStopped = false
//        activityIndicator.stopAnimating()
//        view.addSubview(activityIndicator)
//    }
//
//    func dismissActivityIndicator(activityIndicator: UIActivityIndicatorView) {
//        DispatchQueue.main.async {
//            activityIndicator.stopAnimating()
//            activityIndicator.removeFromSuperview()
//        }
//    }
//
    func createAccount() {
        Auth.auth().createUser(withEmail: self.emailTextField.text!, password: self.passwordTextField.text!) { (user, error) in
            
            if user != nil {
                print("You have successfully signed up")
            } else {
                print("There's an error creating your account.")
                self.displayAlert(alertMessage: "There's an error creating your account.")
            }
        }
    }
}
