//
//  ViewController.swift
//  APISampleJoyce
//
//  Created by Joyce Dizon on 27/12/2018.
//  Copyright © 2018 Novare Technologies. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class ViewController: UIViewController, UIImagePickerControllerDelegate,
    UINavigationControllerDelegate {

    @IBOutlet weak var photoSelection: UIButton!
    let imagePickerController = UIImagePickerController()
    var base64String: String!
    @IBOutlet weak var welcomeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePickerController.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    // MARK: Image Picker Delegate
    
    @IBAction func selectPhoto(_ sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func signOut(_ sender: AnyObject) {
        if let _ = Auth.auth().currentUser {
            do {
                try Auth.auth().signOut()
                navigateToLoginView()
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ imagePicker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            print("Info did not have the required UIImage for the Original Image")
            dismiss(animated: true, completion: nil)
            return
        }
        
        dismiss(animated:true, completion: nil)
        
        guard let imageData = selectedImage.jpegData(compressionQuality: 0.50) else { return }
        base64String = imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        
        navigateToNextView()

    }
    
    // MARK: Navigation
    
    func navigateToNextView() {
        let nextViewController = storyboard?.instantiateViewController(withIdentifier: "TableViewController") as! TableViewController
        nextViewController.imageString = base64String
        navigationController?.pushViewController(nextViewController, animated: true)
    }

    func navigateToLoginView() {
        let loginController = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        navigationController?.pushViewController(loginController, animated: true)
    }
}


