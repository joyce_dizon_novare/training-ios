//
//  TableViewController.swift
//  APISampleJoyce
//
//  Created by Joyce Dizon on 27/12/2018.
//  Copyright © 2018 Novare Technologies. All rights reserved.
//

import UIKit
import Alamofire
import Moya

class TableViewController: UITableViewController {
    
    var TableData:Array<String> = Array<String>()
    var imageString: String?
    var tags = [TagElement]()
    
    // MARK: View life cycle
    
    override func viewDidLoad() {
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        super.viewDidLoad()

        NetworkAdapter.getTags(imageString: imageString!) { imageRecognitionResult in
            self.tags = imageRecognitionResult
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        
        self.refreshControl?.addTarget(self, action: #selector(handleRefresh), for: UIControl.Event.valueChanged)
    }
    
    @objc func handleRefresh(refreshControl: UIRefreshControl) {
        NetworkAdapter.getTags(imageString: imageString!) { imageRecognitionResult in
            self.tags = imageRecognitionResult
        }
        self.tableView.reloadData()
        refreshControl.endRefreshing()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tags.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let tagElement = tags[indexPath.row]
        cell.textLabel?.text = tagElement.tag.en
        
        return cell
    }
}
