//
//  LoginViewController.swift
//  APISampleJoyce
//
//  Created by Joyce Dizon on 17/02/2019.
//  Copyright © 2019 Novare Technologies. All rights reserved.
//

import UIKit
import RealmSwift
import Firebase
import FirebaseAuth
import GoogleSignIn

class LoginViewController: UIViewController {
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    // MARK: View life cycle
        
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: Button Actions

    @IBAction func login(_ sender: UIButton) {
        validateFields()
    }
    
    @IBAction func signup(_ sender: UIButton) {
        navigateToRegister()
    }
    
    @IBAction func reset(_ sender: UIButton) {
        navigateToReset()
    }
    
    // MARK: Field Validation
    
    func validateFields() {
        guard let email = emailTextField.text, let password = passwordTextField.text else {
            return
        }
        
        if !email.isEmpty && !password.isEmpty {
            loginAccount()
        } else {
            displayAlert(alertMessage: "Please fill up all fields.")
        }
        
    }
    
    func displayAlert(alertMessage:String) -> Void {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: "Alert", message: alertMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default) {
                (action:UIAlertAction!) in
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: nil)
                }
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func showActivityIndicator() {
        let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        activityIndicator.center = view.center
        activityIndicator.hidesWhenStopped = false
        activityIndicator.stopAnimating()
        view.addSubview(activityIndicator)
    }
    
    func loginAccount() {
        let email = self.emailTextField.text ?? ""
        let password = self.passwordTextField.text ?? ""
        
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
    
            if let _ = user {
                print("You have successfully logged in")
                self.navigateToHome()
            } else if error != nil {
                print("There's an error logging in your account.")
                self.displayAlert(alertMessage: "There's an error logging in your account.")
            }
        }
    }
    
    // MARK: Navigation
    
    func navigateToHome() {
        let nextViewController = storyboard?.instantiateViewController(withIdentifier: "SelectPhotoController") as! ViewController
        navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    func navigateToRegister() {
        let signupController = storyboard?.instantiateViewController(withIdentifier: "SignupController") as! SignupViewController
        self.present(signupController, animated: true)
    }
    
    func navigateToReset() {
        let resetController = storyboard?.instantiateViewController(withIdentifier: "ResetController") as! ResetViewController
        self.present(resetController, animated: true)
    }
}
