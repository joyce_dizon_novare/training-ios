//
//  ResetViewController.swift
//  APISampleJoyce
//
//  Created by Joyce Dizon on 21/03/2019.
//  Copyright © 2019 Novare Technologies. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class ResetViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    
    // MARK: View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MAKR: Button Actions
    
    @IBAction func cancelReset(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func resetPassword(_ sender: UIButton) {
        validateFields()
    }
    
    // MARK: Validations
    
    func validateFields(){
        if let email = emailTextField.text, email.isEmpty {
            displayAlert(alertMessage: "Please enter your email address.")
            return
        }
        resetPassword()
    }
    
    func displayAlert(alertMessage:String) -> Void {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: "Alert", message: alertMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default) {
                (action:UIAlertAction!) in
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: nil)
                }
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func resetPassword() {
        let email = self.emailTextField.text ?? ""
        Auth.auth().sendPasswordReset(withEmail: email, completion: { (error) in
            
            if error != nil {
                self.displayAlert(alertMessage: "There is an error resetting your password.")
            } else {
                self.displayAlert(alertMessage: "I successfullly sent an email to reset your password.")
                self.emailTextField.text = ""
            }
        })
    }

    // MARK: - Navigation

}
