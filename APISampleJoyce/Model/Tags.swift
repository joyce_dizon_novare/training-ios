//
//  Tags.swift
//  APISampleJoyce
//
//  Created by Joyce Dizon on 05/02/2019.
//  Copyright © 2019 Novare Technologies. All rights reserved.
//

import Foundation

struct ImageRecognitionResult: Codable {
    let result: Result
    let status: Status
    
    enum CodingKeys: String, CodingKey {
        case result = "result"
        case status = "status"
    }
}

struct Result: Codable {
    let tags: [TagElement]
    
    enum CodingKeys: String, CodingKey {
        case tags = "tags"
    }
}

struct TagElement: Codable {
    let confidence: Double
    let tag: TagTag
    
    enum CodingKeys: String, CodingKey {
        case confidence = "confidence"
        case tag = "tag"
    
    }
}

struct TagTag: Codable {
    let en: String

    enum CodingKeys: String, CodingKey {
        case en = "en"

    }
}

struct Status: Codable {
    let text: String
    let type: String

    enum CodingKeys: String, CodingKey {
        case text = "text"
        case type = "type"
    }
}



