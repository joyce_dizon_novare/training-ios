//
//  User.swift
//  APISampleJoyce
//
//  Created by Joyce Dizon on 17/02/2019.
//  Copyright © 2019 Novare Technologies. All rights reserved.
//

import Foundation
import RealmSwift

class User: Object {
    
    @objc dynamic var id = -1
    @objc dynamic var name = ""
    @objc dynamic var email = ""
    @objc dynamic var password = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

extension Realm {
    
    func addUser(_ user: User) {
        do {
            try write {
                add(user)
                print("Successfully added.")
            }
        } catch {
            print("Unable to add user.")
        }
        
    }
}
