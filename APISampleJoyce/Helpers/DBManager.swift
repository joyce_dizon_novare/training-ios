//
//  DBManager.swift
//  APISampleJoyce
//
//  Created by Joyce Dizon on 17/02/2019.
//  Copyright © 2019 Novare Technologies. All rights reserved.
//

import UIKit
import RealmSwift

class DBManager {
    
    private var database:Realm
    static let sharedInstance = DBManager()
    
    private init() {
        
        database = try! Realm()
        
    }
    
    func getDataFromDB() -> Results<User> {
        
        let results: Results<User> = database.objects(User.self)
        return results
    }
    
    func addData(object: User) {
        
        try! database.write {
            database.add(object, update: true)
            print("Added new object")
        }
    }
    
    func deleteAllDatabase()  {
        try! database.write {
            database.deleteAll()
        }
    }
    
    func deleteFromDb(object: User) {
        
        try! database.write {
            database.delete(object)
        }
    }
    
}
